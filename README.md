# Desktop Config

This is my desktop configuration file.

## What I need to run this script?
- A Arch based linux distro
- Internet conection
- Pacman

## Usage

Just run this on your terminal

```
curl https://gitlab.com/Albrigs/desktop-config/-/raw/main/run.sh | bash -
```
## What this does
Install packages, create commands that make easy to call flatpak applications
