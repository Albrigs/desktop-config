#!/bin/bash -
#===============================================================================
#
#          FILE: run.sh
#
#         USAGE: ./run.sh
#
#   DESCRIPTION: This script install things, generate
#
#       OPTIONS: ---
#  REQUIREMENTS: Arch based ditro
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Natan "Albrigs" Fernandes dos Santos,
#  ORGANIZATION: ---
#       CREATED: 04/08/2021 12:27
#      REVISION: ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

SO=$(uname)
if [ $SO != 'Linux' ]; then echo "Run it only on Linux"; exit; fi

is_online=$(ping -c 1 -q 8.8.8.8 >&/dev/null; echo $?)
if [ $is_online != 0 ]; then echo "You are offline, this script will not work."; exit; fi

which pacman
if [ $? != 0 ]; then echo "You don't have pacman, install or use a distro how use that."; exit; fi

LOG_PATH="$HOME/first_install.log"

log_gen(){
<<ARGS
	1 : Exit cmd $?
	2 : Operation (Install/ShortCut/Configure/Nothing)
	3 : Specific Target
ARGS

	NOW=`date '+%d/%m/%Y_%H:%M:%S'`
	RESULT='.'
	if [[ $1 -eq 0 ]]; then
		RESULT='SUCCESS'
	else
		RESULT='ERROR!!'
	fi

	echo "$NOW -- $RESULT --	$1 --	$2 -- $3" >> "$LOG_PATH"
}

PACMAN_PKGS="flatpak discord neovim krita steam firefox git wget curl npm python-pip putty sqlitebrowser ffmpeg xclip xdotool dia lynx nodejs bc whatsapp-for-linux rsync rclone jre8-openjdk-headless jre8-openjdk jdk8-openjdk openjdk8-doc openjdk8-src audacity thunderbird jupyter-notebook traceroute fzf htop python2 python2-pip"

FLATPAKS="com.spotify.Client chat.delta.desktop rest.insomnia.Insomnia com.github.libresprite.LibreSprite io.github.liberodark.OpenDrive com.wps.Office"

PIP_MDL="pyinstaller virtualenv jupyterthemes neovim pyls python-language-server flake8 isort yapf jedi"

NPM_PKGS="npx nextron nativefier"

install_pkgs(){
<<ARGS
	1 : Manager
	2 : List between ""
ARGS
	which $1
	if [[ $? == 0 ]]; then
		cur_cmd=""
		installed_list=0

		#Escolhendo comando que será utilizado dentro do ciclo de instalações
		if [[ $1 == pacman ]]; then
			cur_cmd="sudo pacman -S";
		fi
		if [[ $1 == flatpak ]]; then
			cur_cmd="sudo flatpak install -y flathub";
			installed_list=$(flatpak list --app --columns=application)
		fi
		if [[ $1 == npm ]]; then
			cur_cmd="sudo npm -i -g";
		fi
		if [[ $1 == pip ]]; then
			cur_cmd="pip install";
			installed_list=$(pip freeze)
		fi

		# Loop de instalação
		for e in $2; do
			clear
			if [[ $installed_list == 0 ]]; then
				which $e
			else
				echo $installed_list | grep -q $e
			fi

			if [[ $? != 0 ]]; then
				eval "$cur_cmd $e"
				log_gen $? "Install" $e
			else
				log_gen 0 "Nothing" $e
			fi

		done

	else
		# Gerando erro de gerenciador de pacotes não instalado
		log_gen $? "Check" "$1 -- NOT FOUND"
	fi
}

install_pkgs pacman "$PACMAN_PKGS"
install_pkgs flatpak "$FLATPAKS"
install_pkgs pip "$PIP_MDL"
install_pkgs npm "$NPM_PKGS"

pip2 install neovim
#Atualização de Pacotes
sudo pacman -Syyu

# Inserindo arquivos de comando para facilitar chamada de flatpaks via terminal
flatpak_names=$(flatpak list --app --columns=name)
flat_counter=0
printf %s "$flatpak_names" |
	while read -r e; do
		flat_counter=$(($flat_counter + 1))

		#Extracting DATA
		name=$(echo $e | cut -d '(' -f 1 | sed 's,^ *,,; s, *$,,')
		name_dash=$(echo $name | tr '[:upper:]' '[:lower:]' | tr ' ' -)
		domain=$(flatpak list --app --columns=application | sed -n ${flat_counter}p)

		#Writing Bash File
		file_path="/usr/local/bin/${name_dash}"
		if ! [[ -f $file_path ]]; then
			echo "flatpak run $domain" > $name_dash
			sudo chmod +x $name_dash
			sudo mv $name_dash $file_path
		fi
		log_gen $? "Config" "command $name_dash"
	done

# instala sdk manager
curl -s "https://get.sdkman.io" | bash
source ~/.sdkman/bin/sdkman-init.sh         
sdk install gradle 7.2
# Configurando GIT

which git
if [[ $? == 0 ]]; then
	reanswer=1
	while [[ $reanswer != 0 ]]; do
		clear

		read -p "Enter your git user: " name
		read -p "Enter your git e-mail: " email

		read -p "Ok, your user is $name and e-mail $email (y/n)? " confirmation
		echo $confirmation | grep -iq '^y'
		reanswer=$?

		git config --global --add user.name $name
		log_gen $? "Config" "GIT USER"
		git config --global --add user.email $email
		log_gen $? "Config" "GIT EMAIL"
	done

	#gerando chave RSA
	clear
	ssh-keygen -o -f ~/.ssh/id_rsa
	ssh_gen=$(echo $?)
	message=""

	if [[ $ssh_gen == 0 ]]; then
		clear
		which xclip
		if [[ $? == 0 ]]; then
			message="Your RSA key is on your Transfer Area, add it on your remote repository application."
			xclip -sel clip ~/.ssh/id_rsa.pub
		else
			message="↑ There is your RSA key, copy it and add on your remote repository application."
			cat ~/.ssh/id_rsa.pub
		fi
		log_was_generated=$(echo $?)
		log_gen $log_was_generated "Config" "Show SSH KEY"

		echo $message

		if [[ $log_was_generated == 0 ]]; then
			confirmation=1
			while [[ $confirmation != 0 ]]; do
				read -p "OK? (y/n)" answer
				echo $answer | grep -iq '^y'
				confirmation=$(echo $?)
			done
		fi

	else
		log_gen $ssh_gen "Config" "Generate SSH KEY"
	fi

else
	log_gen $? "Config" "GIT -- NOT FOUND"
fi
